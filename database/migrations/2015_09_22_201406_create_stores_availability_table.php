<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresAvailabilityTable extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('store_availabilty_id');
            $table->integer('store_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('status', 16)->default('active');
            // $table-> 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores_availability');
    }
}