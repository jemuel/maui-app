<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesOffer extends Model
{
    
 	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services_offer';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'service_offer_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


 	/**
     * Get the _stores record associated with the _branches.
     *
     * @return Response
     */
    public function branches()
    {
    	return $this->hasMany('App\Branches');
    }
}
