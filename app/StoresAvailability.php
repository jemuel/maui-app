<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoresAvailability extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stores_availability';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'store_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['available_at'];

}