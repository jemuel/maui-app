<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Security checkpoint.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $users = DB::table("users")->get();
        return view('users.index', ['users' => $users ]);
        return view('users.index', ['users' => $users]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store($userId, $store)
    {
        //
        $user = DB::table('users')
                ->where('user_id', $userId)
                ->get();

        $branchCount = DB::table('stores as s')
                    ->leftjoin('branches as b', 's.store_id', '=', 'b.store_id')
                    ->where('s.user_id', $userId)
                    ->count();

        $checkinCount = DB::table('check_ins as c')
                    ->join('branches as b', 'c.branch_id', '=', 'b.branch_id')
                    ->join('stores as s', 'b.store_id', '=', 's.store_id')
                    ->where('s.user_id', $userId)
                    ->where('c.created_at', date('Y-m-d h:i:s'))
                    ->count();

        $branches = DB::table('branches as b')
                ->leftjoin('stores as s', 'b.store_id', '=', 's.store_id')
                ->select('b.*', 's.name as storeName')
                ->where('b.store_id', $store)
                ->get();


        return view('users.branches', [ 'data' => array(
            'user'          => $user,
            'branchCount'   => $branchCount,
            'checkinCount'  => $checkinCount,
            'branches'      => $branches
        ) ]);
    }

    /**
     * Display stores from specific user
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeShow($userId)
    {
        $user = DB::table('users')
                ->where('user_id', $userId)
                ->get();

        $branchCount = DB::table('stores as s')
                    ->leftjoin('branches as b', 's.store_id', '=', 'b.store_id')
                    ->where('s.user_id', $userId)
                    ->count();

        $checkinCount = DB::table('check_ins as c')
                    ->join('branches as b', 'c.branch_id', '=', 'b.branch_id')
                    ->join('stores as s', 'b.store_id', '=', 's.store_id')
                    ->where('s.user_id', $userId)
                    ->where('c.created_at', date('Y-m-d h:i:s'))
                    ->count();

        $stores = DB::table('stores')
                ->select(DB::raw('mss_stores.*, (SELECT COUNT(b.branch_id) FROM mss_branches b WHERE b.store_id = mss_stores.store_id) AS cntBranches'))
                ->where('stores.user_id', $userId)
                ->get();

        return view('users.stores', [ 'data' => array(
            'user'          => $user,
            'branchCount'   => $branchCount,
            'checkinCount'  => $checkinCount,
            'stores'        => $stores
        ) ]);
    }

    /**
     * Display checkins from specific user's branch
     *
     * @param  Request  $request
     * @return Response
     */
    public function checkinShow($userId,  $branchId)
    {
        $user = DB::table('users')
                ->where('user_id', $userId)
                ->get();

        $branchCount = DB::table('stores as s')
                    ->leftjoin('branches as b', 's.store_id', '=', 'b.store_id')
                    ->where('s.user_id', $userId)
                    ->count();

        $checkinCount = DB::table('check_ins as c')
                    ->join('branches as b', 'c.branch_id', '=', 'b.branch_id')
                    ->join('stores as s', 'b.store_id', '=', 's.store_id')
                    ->where('s.user_id', $userId)
                    ->where('c.created_at', date('Y-m-d h:i:s'))
                    ->count();

        $checkIns = DB::table('check_ins as c')
                ->leftjoin('branches as b', 'b.branch_id', '=', 'c.branch_id')
                ->leftjoin('stores as s', 's.store_id', '=', 'b.store_id')
                ->leftjoin('services as se', 'se.service_id', '=', 'c.service_id')
                ->select('c.*', 's.name as storeName', 'b.address as address', 'se.name as serviceName')
                ->where('c.branch_id', $branchId)
                ->get();


        return view('users.checkins', [ 'data' => array(
            'user'          => $user,
            'branchCount'   => $branchCount,
            'checkinCount'  => $checkinCount,
            'checkIns'        => $checkIns
        ) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($userId)
    {
        //
        $user = DB::table('users')
                ->where('user_id', $userId)
                ->get();

        $branchCount = DB::table('stores as s')
                    ->leftjoin('branches as b', 's.store_id', '=', 'b.store_id')
                    ->where('s.user_id', $userId)
                    ->count();

        $checkinCount = DB::table('check_ins as c')
                    ->join('branches as b', 'c.branch_id', '=', 'b.branch_id')
                    ->join('stores as s', 'b.store_id', '=', 's.store_id')
                    ->where('s.user_id', $userId)
                    ->where('c.created_at', date('Y-m-d h:i:s'))
                    ->count();


        return view('users.show', [ 'data' => array(
            'user'          => $user,
            'branchCount'   => $branchCount,
            'checkinCount'  => $checkinCount
        ) ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function showBranches($userId)
    {
        //
        //. echo $userId;
        //
        return view('users.show');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}