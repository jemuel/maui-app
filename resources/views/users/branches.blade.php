@extends('layout')

@section('content')

    @include('_partial.header')
    
    @include('_partial.sidebar_left')

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="fa fa-ok-sign"></i>
                    Success!
                </h4>
                <p>{{ Session::get('flash_message') }}</p>
            </div>
            @endif
            <div class="row">

                <aside class="profile-nav col-lg-3">
                    <section class="panel">
                        <div class="user-heading round">
                            <a href="#">
                                <img alt="" src="{{ url() }}/themes/flatlab/img/unknown.gif">
                            </a>
                            <h1>{{ $data['user'][0]->name }}</h1>
                            <p>{{ $data['user'][0]->email }}</p>
                        </div>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url() }}/users/{{ $data['user'][0]->user_id }}"> <i class="fa fa-user"></i> Profile</a></li>
                            <li><a href="profile-edit.html"> <i class="fa fa-edit"></i> Edit profile</a></li>
                            <li class="active"><a href="{{ url() }}/users/{{ $data['user'][0]->user_id }}/stores"> <i class="fa fa-calendar"></i> Stores </li>
                        </ul>
                    </section>
                </aside>

                <aside class="profile-info col-lg-9">
                    <div class="row state-overview">
                        <div class="col-lg-5 col-sm-6">
                            <section class="panel">
                                <div class="symbol terques">
                                    <i class="fa fa-building-o"></i>
                                </div>
                                <div class="value">
                                    <h1 class="count">{{ $data['branchCount'] }}</h1>
                                    <p>Branch</p>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            <section class="panel">
                                <div class="symbol red">
                                    <i class="fa fa-check-square-o"></i>
                                </div>
                                <div class="value">
                                    <h1 class=" count2">{{ $data['checkinCount'] }}</h1>
                                    <p>Checked In</p>
                                </div>
                            </section>
                        </div>
                    </div>
                    
                    <section class="panel">
                        <div class="bio-graph-heading">
                            Aliquam ac magna metus. Nam sed arcu non tellus fringilla fringilla ut vel ispum. Aliquam ac magna metus.
                         </div>
                        <div class="panel-body bio-graph-info">
                            <h1>{{ $data['branches'][0]->storeName }}</h1>
                            <div class="row">
                                <div class="col-lg-12">
                                    <section class="panel">
                                        <table class="table table-striped table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th><i class="fa fa-map-marker"></i> Branch Address</th>
                                                    <th class="hidden-phone"><i class="fa fa-phone"></i> Phone Number</th>
                                                    <th><i class="fa fa-clock-o"></i> Store Hours</th>
                                                    <th><i class="fa fa-calendar-o"></i> Created At</th>
                                                    <th><i class=" fa fa-info-circle"></i> Status</th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @if($data['branches'])
                                                @foreach ($data['branches'] as $branch)
                                                <tr>
                                                    <td><a href="{{ url() }}/users/{{ $data['user'][0]->user_id }}/branches/{{ $branch->branch_id }}">{{ $branch->address }}</a></td>
                                                    <td class="hidden-phone">{{ $branch->telephone_number }}</td>
                                                    <td>{{ $branch->store_hours }}</td>
                                                    <td>{{ date('m/d/Y h:i A', strtotime($branch->created_at)) }}</td>
                                                    <td><span class="label label-info label-mini">{{ $branch->status }}</span></td>
                                                </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                        </table>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>

                </aside>
            </div>
        </section>
    </section>
    <!--main content end-->

    @include('_partial.slidebar_right')

    @include('_partial.footer')
@endsection