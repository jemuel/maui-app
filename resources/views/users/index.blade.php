@extends('layout')

@section('content')

    @include('_partial.header')
    
    @include('_partial.sidebar_left')

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="fa fa-ok-sign"></i>
                    Success!
                </h4>
                <p>{{ Session::get('flash_message') }}</p>
            </div>
            @endif
            <ul class="directory-list">
                <li><a href="#">a</a></li>
                <li><a href="#">b</a></li>
                <li><a href="#">c</a></li>
                <li><a href="#">d</a></li>
                <li><a href="#">e</a></li>
                <li><a href="#">f</a></li>
                <li><a href="#">g</a></li>
                <li><a href="#">h</a></li>
                <li><a href="#">i</a></li>
                <li><a href="#">j</a></li>
                <li><a href="#">k</a></li>
                <li><a href="#">l</a></li>
                <li><a href="#">m</a></li>
                <li><a href="#">n</a></li>
                <li><a href="#">o</a></li>
                <li><a href="#">p</a></li>
                <li><a href="#">q</a></li>
                <li><a href="#">r</a></li>
                <li><a href="#">s</a></li>
                <li><a href="#">t</a></li>
                <li><a href="#">u</a></li>
                <li><a href="#">v</a></li>
                <li><a href="#">w</a></li>
                <li><a href="#">x</a></li>
                <li><a href="#">y</a></li>
                <li><a href="#">z</a></li>
            </ul>
            <div class="directory-info-row">
                <div class="row">
                    @foreach ($users as $user)
                    <div class="col-md-6 col-sm-6">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="media">
                                    <a href="users/id/{{ $user->user_id }}" class="pull-left">
                                    <a href="{{ url() }}/users/{{ $user->user_id }}" class="pull-left">
                                    <a href="{{ url() }}/users/{{ $user->user_id }}" class="pull-left">
                                        <img alt="" src="{{ url() }}/themes/flatlab/img/unknown.gif" class="thumb media-object">
                                    </a>
                                    <div class="media-body">
                                        <h4>{{ $user->name }}</h4>
                                        <ul class="social-links">
                                            <li><a data-original-title="Facebook" href="" class="tooltips" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a data-original-title="Twitter" href="" class="tooltips" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a data-original-title="LinkedIn" href="" class="tooltips" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-linkedin"></i></a></li>
                                            <li><a data-original-title="Skype" href="" class="tooltips" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-skype"></i></a></li>
                                        </ul>
                                        <address>
                                            Email: {{ $user->email }}<br>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach;
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

    @include('_partial.slidebar_right')

    @include('_partial.footer')
@endsection