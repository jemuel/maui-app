<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>Coming Soon </title>
    <!-- Bootstrap core CSS -->
    <link href="{{ url() }}/themes/flatlab/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url() }}/themes/flatlab/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{ url() }}/themes/flatlab/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- coming soon styles -->
    <link href="{{ url() }}/themes/flatlab/css/soon.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="{{ url() }}/themes/flatlab/css/style.css" rel="stylesheet">
    <link href="{{ url() }}/themes/flatlab/css/style-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="cs-bg">
    <!-- START HEADER -->
    <section id="header">
        <div class="container">
            <header>
                <!-- HEADLINE -->
                <a class="logo floatless" href="index.html"><img src="http://beta.molavenet.com/nikki/mauisalon/public/themes/flatlab/img/maui-salon.png" alt="Maui Salon"></a>
                <h1 > COMING SOON</h1>
                <br/>
				<h3> WE ARE BUSY BEES... </h3>
				<br/>
                <p> IN JUST A BIT. YOU WILL BE ALBE TO CHECK IN TO ANY MAUI AUTHORIZED SALON ACROSS THE COUNTRY FOR LASER TEETH WHITENING, BOTOX, LASH EXTENSIONS, HAIR EXTENSIONS AND MORE! </p>
            </header>
            <!-- START TIMER -->
            <div id="timer" data-animated="FadeIn">
                <p id="message"></p>
                <div id="days" class="timer_box"></div>
                <div id="hours" class="timer_box"></div>
                <div id="minutes" class="timer_box"></div>
                <div id="seconds" class="timer_box"></div>
            </div>
            <!-- END TIMER -->
            <div class="col-lg-4 col-lg-offset-4 mt centered">
                <h4> LET ME KNOW WHEN IT LAUNCH</h4>
                <form class="form-inline" role="form">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                    </div>
                    <button type="submit" class="btn btn-danger">Submit</button>
                </form>
            </div>
            
        </div>
    </section>
    <!-- END HEADER -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ url() }}/themes/flatlab/js/jquery.js"></script>
    <script type="text/javascript" src="{{ url() }}/themes/flatlab/js/modernizr.custom.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/bootstrap.min.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/soon/plugins.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/soon/custom.js"></script>
</body>
<!-- END BODY -->
</html>